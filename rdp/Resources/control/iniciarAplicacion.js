var VentanaMenu = require('menu');

function Iniciar(){
	var VentanaPrincipal;
	var botonAbrir;
	
	VentanaPrincipal = Ti.UI.createWindow({
		backgroundColor:"#000"
	});
	
	var vista = Ti.UI.createView({
		top:120,	
		left:40,
		right:40,
		//backgroundColor:'#fff'
	}); 
	
	var botonAbrir = Ti.UI.createButton({
		title: 'Bienvenido',
		image:'../logoinicio.png',
		width:"55%",
		height:60,
		top:"40%"
	});
	
	botonAbrir.addEventListener('click', function(){
		VentanaMenu.mostrarmenu();
		VentanaPrincipal.close();
		VentanaPrincipal.remove(botonAbrir);
		botonAbrir = null;
		VentanaPrincipal = null;
		
	});	
	
	
	vista.add(botonAbrir);
	VentanaPrincipal.add(vista);
	VentanaPrincipal.open();
}

exports.Iniciar = Iniciar;