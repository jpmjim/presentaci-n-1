var mostrarsubmenu = require('submenuDestino');
var mostrarsubmenuservicio = require('submenuservicio');
var mostrarsubmenuprecio = require('submenuprecio');
var mostrarsubmenureserva = require('submenureserva');

function mostrarmenu() {
	
	var ventanaMenu = Ti.UI.createWindow({
		backgroundColor:'#0A2A29'
	});
	
	//vistas
	var vista = Ti.UI.createView({
		top:120,	
		left:40,
		right:40,
		//backgroundColor:'#fff'
	});
	
	//botones
	var botonDestino = Ti.UI.createButton({
		title:'Destinos',
		image:'destino.png',
		top:10,
		left:10,
		width:'96%',
		height:'20%'
	});
	
	botonDestino.addEventListener('click', function(e){
		mostrarsubmenu.submenu();
	});
	
	var botonServicio = Ti.UI.createButton({
		title:'Servicios',
		image:'servicios.png',
		top:140,
		left:10,
		width:'96%',
		height:'20%'
	});
	
	botonServicio.addEventListener('click', function(e){
		mostrarsubmenuservicio.submenu();
	});
	
	var botonPrecio = Ti.UI.createButton({
		title:'Precios',
		image:'precio.png',
		top:270,
		left:10,
		width:'96%',
		height:'20%'
	});
	
	botonPrecio.addEventListener('click', function(e){
		mostrarsubmenuprecio.submenu();
	});
	
	var botonReserva = Ti.UI.createButton({
		title:'Reserva de Pasaje',
		image:'reserva.png',
		top:400,
		left:10,
		width:'96%',
		height:'20%'
	});
	
	botonReserva.addEventListener('click', function(e){
		mostrarsubmenureserva.submenu();
	});
	
	//vista de imagenes
	var cab = Ti.UI.createImageView({
		top:'0',
		image:'logo.png',
		width:'100%',
		height:'15%',
		backgroundColor:'#000'
	});
	
	//open
	
	vista.add(botonDestino);
	vista.add(botonServicio);
	vista.add(botonPrecio);
	vista.add(botonReserva);
	ventanaMenu.add(cab);
	ventanaMenu.add(vista);
	ventanaMenu.open();

}

exports.mostrarmenu = mostrarmenu;