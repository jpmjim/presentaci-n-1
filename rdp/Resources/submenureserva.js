function submenu(){
	//ventanas
	var ventanaSubmenu = Ti.UI.createWindow({
		backgroundColor:'#0A2A29'
	});
	
	//cabecera submenu
	var cabeceraSubmenu = Ti.UI.createImageView({
		top:'0',
		image:'logo.png',
		width:'80%',
		left: '0',
		height:'15%',
		backgroundColor:'#000'
	});	
	
	//boton volver al menu
	var vistaBoton = Ti.UI.createView({
		width:'20%',
		height:'15%',
		top:0,	
		right:0,
		backgroundColor:'#000'
	});
	
	var botonMenu = Ti.UI.createButton({
		image:'atras.png',
		right: 10
		//top: 0,
		//left:0,
		//width:'50%'
	});
	
	var webReserva = Ti.UI.createWebView({
		url:'/jquery/registro.html'
	}) ;
	
	botonMenu.addEventListener('click', function(){
		ventanaSubmenu.close();
		ventanaSubmenu.remove(botonMenu);
		botonMenu = null;
		ventanaSubmenu = null;
	});
	
	ventanaSubmenu.add(webReserva);
	vistaBoton.add(botonMenu);
	ventanaSubmenu.add(vistaBoton);
	ventanaSubmenu.add(cabeceraSubmenu);
	ventanaSubmenu.open();
	return ventanaSubmenu;
}

exports.submenu = submenu;
